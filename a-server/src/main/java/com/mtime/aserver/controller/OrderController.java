package com.mtime.aserver.controller;

import com.mtime.aserver.bean.dto.StockDTO;
import com.mtime.aserver.common.ResponseMsg;
import com.mtime.aserver.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单接入
 *
 * @author zhukai
 * @program a-server
 * @class OrderController
 * @create 2020-02-15 17:11
 **/
@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/v1/order/submit")
    ResponseMsg orderSubmit() {
        orderService.orderSubmit();
        return new ResponseMsg("200", "订单提交成功", "");
    }
}
