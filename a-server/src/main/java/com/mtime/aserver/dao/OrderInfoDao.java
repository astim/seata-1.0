package com.mtime.aserver.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mtime.aserver.bean.po.OrderInfoPO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 商品
 *
 * @author zhukai
 * @program OrderInfoDao.java
 * @class GoodsDao
 * @create 2020/2/15 16:52
 **/
@Mapper
@Repository
public interface OrderInfoDao extends BaseMapper<OrderInfoPO> {
}
