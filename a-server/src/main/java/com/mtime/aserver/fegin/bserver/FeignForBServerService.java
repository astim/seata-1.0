package com.mtime.aserver.fegin.bserver;

import com.mtime.aserver.bean.dto.StockDTO;
import com.mtime.aserver.common.ResponseMsg;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 用户中心Fegin接口
 *
 * @author zhukai
 * @program venus-order-center
 * @class userCenterFeignService
 * @create 2020-02-11 10:43
 **/
@Service
@FeignClient(name = "b-server",fallback = FeignForBServerServiceImpl.class)
public interface FeignForBServerService {

    /**
     * 添加存存
     *
     * @param stockDTO 库存对象
     * @return "StockDTO"
     * @method changeStock
     * @author zhukai
     * @date 2020/2/11 16:00
     */
    @PostMapping("/v1/stock/change")
    ResponseMsg changeStock(StockDTO stockDTO);

}
