package com.mtime.aserver.fegin.cserver;

import com.mtime.aserver.bean.dto.AccountDTO;
import com.mtime.aserver.bean.dto.StockDTO;
import com.mtime.aserver.common.ResponseMsg;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 用户中心Fegin接口
 *
 * @author zhukai
 * @program venus-order-center
 * @class userCenterFeignService
 * @create 2020-02-11 10:43
 **/
@Service
@FeignClient(name = "c-server",fallback = FeignForCServerServiceImpl.class)
public interface FeignForCServerService {

    /**
     * 添加存存
     *
     * @param accountDTO 账户对象
     * @return "ResponseMsg"
     * @method changeAmount
     * @author zhukai
     * @date 2020/2/11 16:00
     */
    @PostMapping("/v1/account/change")
    ResponseMsg changeAccount(AccountDTO accountDTO);

}
