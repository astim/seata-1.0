package com.mtime.aserver.fegin.cserver;

import com.mtime.aserver.bean.dto.AccountDTO;
import com.mtime.aserver.bean.dto.StockDTO;
import com.mtime.aserver.common.ResponseMsg;
import org.springframework.stereotype.Service;

/**
 * 用户中心服务调用-熔断降级
 *
 * @author zhukai
 * @program venus-order-center
 * @class UserCenterFeignServiceImpl
 * @create 2020-02-11 17:38
 **/
@Service
public class FeignForCServerServiceImpl implements FeignForCServerService {

    /**
     * 熔断返回
     *
     * @param accountDTO
     * @return "com.mtime.aserver.common.ResponseMsg"
     * @method changeStock
     * @author zhukai
     * @create 2020/2/15 16:26
     */
    @Override
    public ResponseMsg changeAccount(AccountDTO accountDTO) {
        return new ResponseMsg("100", "服务中断", "");
    }
}
