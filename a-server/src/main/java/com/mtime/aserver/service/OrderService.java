package com.mtime.aserver.service;

/**
 * 订单接口
 *
 * @author zhukai
 * @program a-server
 * @class OrderService
 * @create 2020-02-15 16:28
 **/
public interface OrderService {

    /**
     * 订单提交
     *
     * @param
     * @return "void"
     * @method orderSubmit
     * @author zhukai
     * @create 2020/2/15 16:29
     */
    void orderSubmit();
}
