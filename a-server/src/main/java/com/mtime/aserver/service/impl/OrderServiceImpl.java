package com.mtime.aserver.service.impl;

import com.mtime.aserver.bean.dto.AccountDTO;
import com.mtime.aserver.bean.dto.StockDTO;
import com.mtime.aserver.bean.po.OrderInfoPO;
import com.mtime.aserver.dao.OrderInfoDao;
import com.mtime.aserver.fegin.bserver.FeignForBServerService;
import com.mtime.aserver.fegin.cserver.FeignForCServerService;
import com.mtime.aserver.service.OrderService;
import io.seata.spring.annotation.GlobalTransactional;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 订单实现
 *
 * @author zhukai
 * @program a-server
 * @class OrderServiceImpl
 * @create 2020-02-15 16:28
 **/
@Service
public class OrderServiceImpl implements OrderService {


    @Autowired
    private FeignForBServerService feignForBServerService;

    @Autowired
    private FeignForCServerService feignForCServerService;

    @Autowired
    private OrderInfoDao orderInfoDao;

    /**
     * 订单提交
     *
     * @param
     * @return "void"
     * @method orderSubmit
     * @author zhukai
     * @create 2020/2/15 16:30
     */
    @Override
    @GlobalTransactional
    public void orderSubmit() {
        // 添加订单信息
        OrderInfoPO orderInfoPO = new OrderInfoPO();
        orderInfoPO.setQuantity(1);
        orderInfoDao.insert(orderInfoPO);

        // 修改b-server库存信息
        StockDTO stockDTO = new StockDTO();
        stockDTO.setId(1);
        stockDTO.setStock(1);
        feignForBServerService.changeStock(stockDTO);

        // 修改c-server账户信息
        AccountDTO accountDTO=new AccountDTO();
        accountDTO.setId(1);
        accountDTO.setAmount(10);
        feignForCServerService.changeAccount(accountDTO);
        int i=1/0;
    }
}
