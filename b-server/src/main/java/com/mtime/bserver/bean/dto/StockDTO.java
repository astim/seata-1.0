package com.mtime.bserver.bean.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 库存传输对象
 *
 * @author zhukai
 * @program StockDTO.java
 * @class StockDTO
 * @create 2020/2/15 16:22
 **/
@Data
public class StockDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 商品ID
     */
    private Integer id;

    /**
     * 库存数量
     */
    private Integer stock;
}
