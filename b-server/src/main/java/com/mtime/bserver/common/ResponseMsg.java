package com.mtime.bserver.common;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * show TODO
 *
 * @author zhukai
 * @program a-server
 * @class ResponseMsg
 * @create 2020-02-15 16:18
 **/
@Data
@AllArgsConstructor
public class ResponseMsg {
    /**
     * 返回码
     */
    String code;
    /**
     * 错误信息
     */
    String msg;
    /**
     * 返回数据
     */
    String data;


}
