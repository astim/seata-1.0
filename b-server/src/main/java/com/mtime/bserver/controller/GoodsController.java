package com.mtime.bserver.controller;

import com.mtime.bserver.common.ResponseMsg;
import com.mtime.bserver.bean.dto.StockDTO;
import com.mtime.bserver.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 商品接口接入
 *
 * @author zhukai
 * @program b-server
 * @class GoodsController
 * @create 2020-02-15 16:55
 **/
@RestController
public class GoodsController {
    @Autowired
    private GoodsService goodsService;

    /**
     * 变更商品库存
     *
     * @param stockDTO
     * @return "com.mtime.bserver.common.ResponseMsg"
     * @method changeStock
     * @author zhukai
     * @create 2020/2/15 16:57
     */
    @PostMapping("/v1/stock/change")
    ResponseMsg changeStock(@RequestBody StockDTO stockDTO) {
        goodsService.changeStock(stockDTO);
        return new ResponseMsg("200", "库存修改成功", "");
    }

}
