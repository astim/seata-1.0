package com.mtime.bserver.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mtime.bserver.bean.po.GoodsPO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 商品
 *
 * @author zhukai
 * @program OrderInfoDao.java
 * @class GoodsDao
 * @create 2020/2/15 16:52
 **/
@Mapper
@Repository
public interface GoodsDao extends BaseMapper<GoodsPO> {
    /**
     * 变更库存
     *
     * @param id
     * @param stock
     * @return "java.lang.Integer"
     * @method modifyStock
     * @author zhukai
     * @create 2020/2/15 18:03
     */
    Integer modifyStock(int id, int stock);
}
