package com.mtime.bserver.service;

import com.mtime.bserver.bean.dto.StockDTO;

/**
 * 商品接口
 *
 * @author zhukai
 * @program b-server
 * @class GoodsService
 * @create 2020-02-15 16:34
 **/
public interface GoodsService {

    /**
     * 库存变动
     *
     * @param stockDTO
     * @return "void"
     * @method changeStock
     * @author zhukai
     * @create 2020/2/15 16:39
     */
    void changeStock(StockDTO stockDTO);
}
