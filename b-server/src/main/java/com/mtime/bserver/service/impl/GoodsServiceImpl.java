package com.mtime.bserver.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.mtime.bserver.bean.dto.StockDTO;
import com.mtime.bserver.bean.po.GoodsPO;
import com.mtime.bserver.dao.GoodsDao;
import com.mtime.bserver.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 商品接口实现
 *
 * @author zhukai
 * @program b-server
 * @class GoodsServiceImpl
 * @create 2020-02-15 16:35
 **/
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    /**
     * 库存变动
     *
     * @param stockDTO
     * @return "void"
     * @method changeStock
     * @author zhukai
     * @create 2020/2/15 16:39
     */
    @Override
    public void changeStock(StockDTO stockDTO) {
        /**
         * 更新库存
         */
        goodsDao.modifyStock(stockDTO.getId(), stockDTO.getStock());
        int i = 1 / 0;
    }
}
