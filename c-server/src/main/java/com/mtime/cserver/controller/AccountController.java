package com.mtime.cserver.controller;

import com.mtime.cserver.bean.dto.AccountDTO;
import com.mtime.cserver.common.ResponseMsg;
import com.mtime.cserver.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 账户接口接入
 *
 * @author zhukai
 * @program b-server
 * @class GoodsController
 * @create 2020-02-15 16:55
 **/
@RestController
public class AccountController {
    @Autowired
    private AccountService accountService;

    /**
     * 变更账户额度
     *
     * @param accountDTO
     * @return "com.mtime.bserver.common.ResponseMsg"
     * @method changeStock
     * @author zhukai
     * @create 2020/2/15 16:57
     */
    @PostMapping("/v1/account/change")
    ResponseMsg changeAccount(@RequestBody AccountDTO accountDTO) {
        accountService.changeAccount(accountDTO);
        return new ResponseMsg("200", "账户修改成功", "");
    }

}
