package com.mtime.cserver.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mtime.cserver.bean.po.AccountPO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 账户
 *
 * @author zhukai
 * @program OrderInfoDao.java
 * @class GoodsDao
 * @create 2020/2/15 16:52
 **/
@Mapper
@Repository
public interface AccountDao extends BaseMapper<AccountPO> {
    /**
     * 变更账户
     *
     * @param id
     * @param amount
     * @return "java.lang.Integer"
     * @method modifyAccount
     * @author zhukai
     * @create 2020/2/15 18:03
     */
    Integer modifyAccount(int id, int amount);
}
