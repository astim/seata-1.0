package com.mtime.cserver.service;


import com.mtime.cserver.bean.dto.AccountDTO;

/**
 * 账户接口
 *
 * @author zhukai
 * @program c-server
 * @class AccountService
 * @create 2020-02-15 16:34
 **/
public interface AccountService {

    /**
     * 账户变动
     *
     * @param accountDTO
     * @return "void"
     * @method changeAccount
     * @author zhukai
     * @create 2020/2/15 16:39
     */
    void changeAccount(AccountDTO accountDTO);
}
