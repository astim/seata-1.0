package com.mtime.cserver.service.impl;

import com.mtime.cserver.bean.dto.AccountDTO;
import com.mtime.cserver.dao.AccountDao;
import com.mtime.cserver.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 账户接口实现
 *
 * @author zhukai
 * @program b-server
 * @class GoodsServiceImpl
 * @create 2020-02-15 16:35
 **/
@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountDao accountDao;

    /**
     * 账户变动
     *
     * @param accountDTO
     * @return "void"
     * @method changeAccount
     * @author zhukai
     * @create 2020/2/15 16:39
     */
    @Override
    public void changeAccount(AccountDTO accountDTO) {
        /**
         * 更新库存
         */
        accountDao.modifyAccount(accountDTO.getId(), accountDTO.getAmount());
        int i=1/0;
    }
}
