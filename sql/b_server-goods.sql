CREATE DATABASE `b_server` /*!40100 COLLATE 'utf8_general_ci' */
CREATE TABLE `goods` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '商品ID',
	`good_name` VARCHAR(50) NOT NULL DEFAULT '0' COMMENT '商品名称',
	`good_stock` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '商品库存',
	PRIMARY KEY (`id`)
)
COMMENT='商品表'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=2
;
/*
* 插入一条测试记录
*/
insert into b_server.goods(good_name,good_stock) values ('小饼干',6);
