CREATE TABLE `undo_log` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT 'increment id',
	`branch_id` BIGINT(20) NOT NULL COMMENT 'branch transaction id',
	`xid` VARCHAR(100) NOT NULL COMMENT 'global transaction id',
	`context` VARCHAR(128) NOT NULL COMMENT 'undo_log context,such as serialization',
	`rollback_info` LONGBLOB NOT NULL COMMENT 'rollback info',
	`log_status` INT(11) NOT NULL COMMENT '0:normal status,1:defense status',
	`log_created` DATETIME NOT NULL COMMENT 'create datetime',
	`log_modified` DATETIME NOT NULL COMMENT 'modify datetime',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `ux_undo_log` (`xid`, `branch_id`)
)
COMMENT='AT transaction mode undo table'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=343
;
